#! /usr/bin/python3
from PIL import Image
import pymysql.cursors
import shutil
from pathlib import Path
from PIL import ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True

def watermark_with_transparency(input_image_path,output_image_path):
    watermark_image_path='copy.png' #wotermark image
    if output_image_path!='none':
        shutil.copy(input_image_path,output_image_path)
    base_image = Image.open(input_image_path)
    watermark = Image.open(watermark_image_path).convert("RGBA")
    width, height = base_image.size

    transparent = Image.new('RGB', (width, height), (0, 0, 0, 0))
    transparent.paste(base_image, (0, 0))
    transparent.paste(watermark, (width-148, height-58), mask=watermark)
        #transparent.show()
    transparent.save(input_image_path)
    #print(input_image_path)
if __name__ == '__main__':
# Connect to the database
    connection = pymysql.connect(host='localhost',
                             user='userventhelp',
                             password='P)rXeE7@B7zlT2&',
                             db='dbventhelp',
                             charset='utf8',
                             cursorclass=pymysql.cursors.DictCursor)
try:
    with connection.cursor() as cursor:
        #  тут указал категории товаров в которых есть еще подкатегории ,84,73,66,42,34,33
        #sql="SELECT `ID` FROM `b_iblock_section` WHERE  `IBLOCK_SECTION_ID` IN (50,149,150,151,152,153,154,155,156)"
        #cursor.execute(sql)
        #result_all = cursor.fetchall()
        #s = []
        #for lis in result_all:
              #s.append(lis['ID'])
        #sql = "SELECT `ID`, `DETAIL_PICTURE` FROM `b_iblock_element` WHERE `IBLOCK_SECTION_ID` IN (%s)" % ",".join(map(str,s))

        sql = "SELECT `ID`, `DETAIL_PICTURE` FROM `b_iblock_element` WHERE `IBLOCK_ID`=1"
        cursor.execute(sql)
        result = cursor.fetchall()
        for tov in result:

        #  работаем с фотографией  товара
            if tov['DETAIL_PICTURE']:

                sql = "SELECT `SUBDIR`, `FILE_NAME` FROM `b_file` WHERE `ID`=%s"
                cursor.execute(sql, (tov['DETAIL_PICTURE'],))
                result = cursor.fetchone()
                if result:
                    url_file="./upload/"+result['SUBDIR']+"/"+result['FILE_NAME']
                    url_file2="./upload/"+result['SUBDIR']+"/old"+result['FILE_NAME']

                my_file = Path(url_file2)

                if my_file.is_file():
                    #print(url_file2)
                    r=5
                else:
                    print(tov['DETAIL_PICTURE'])
                    watermark_with_transparency(url_file,url_file2)
                    # получаем картинки галереии товара
                    sql = "SELECT `VALUE` FROM `b_iblock_element_property` WHERE `IBLOCK_ELEMENT_ID`=%s AND `IBLOCK_PROPERTY_ID`=13"
                    cursor.execute(sql, (tov['ID']))
                    result_dop = cursor.fetchall()
                    # работаем с каждым файлом отдельно
                    for val in result_dop:
                        sql = "SELECT `SUBDIR`, `FILE_NAME` FROM `b_file` WHERE `ID`=%s"
                        cursor.execute(sql, (val['VALUE'],))
                        result_file = cursor.fetchone()
                        url_file_s = "./upload/" + result_file['SUBDIR'] + "/" + result_file['FILE_NAME']
                        watermark_with_transparency(url_file_s, 'none')
finally:
    connection.close()