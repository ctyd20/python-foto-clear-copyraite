#! /usr/bin/python3
from PIL import Image
import pymysql


def watermark_with_transparency(input_image_path,
                                watermark_image_path):
    output_image_path=input_image_path
    base_image = Image.open(input_image_path)
    watermark = Image.open(watermark_image_path).convert("RGBA")
    #watermark =watermark.convert('RGB')
    width, height = base_image.size

    transparent = Image.new('RGB', (width, height), (0, 0, 0, 0))
    transparent.paste(base_image, (0, 0))
    transparent.paste(watermark, (width-200, height-40), mask=watermark)
    transparent.show()
    transparent.save(output_image_path)


if __name__ == '__main__':

    watermark_with_transparency('942-941-flensburg_o5071216.jpg', 'c-7.png')
